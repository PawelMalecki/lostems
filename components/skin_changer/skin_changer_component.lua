local SkinChangerComponent = class()

function SkinChangerComponent:initialize()
  self._skin_info = radiant.entities.get_entity_data(self._entity, 'lostems:skin_info')
  if self._skin_info and self._skin_info.skins then
    self._sv.current_skin = self._skin_info.default_skin or 'default'
    self._sv.attached_entities = {}
    self.__saved_variables:mark_changed()

    self._created = false
    self._create_skin = false
  else
    self._entity:remove_component('lostems:skin_changer')
  end
end

--[[--
function SkinChangerComponent:activate()
  if radiant.is_server then
    self._removed_from_world_listener = radiant.events.listen(self._entity, 'stonehearth:on_removed_from_world', function()
      print('SkinChangerComponent:on_removed_from_world(): ' .. tostring(self._entity))
      self:apply_default_skin()
    end)
  end
end
--]]--

function SkinChangerComponent:create()
  -- print('SkinChangerComponent:create(): ' .. tostring(self._entity))
  self._created = true
  if self._create_skin then
    self:apply_skin(self._create_skin, true, true)
    self._create_skin = false
  else
    self:apply_default_skin(true)
  end
end

function SkinChangerComponent:restore()
  -- print('SkinChangerComponent:restore(): ' .. tostring(self._entity))
  self._created = true
end

function SkinChangerComponent:destroy()
  if self._ghost_listener then
    self._ghost_listener:destroy()
    self._ghost_listener = nil
  end
  --[[--
  if self._removed_from_world_listener then
    self._removed_from_world_listener:destroy()
    self._removed_from_world_listener = nil
  end
  --]]--
  local render_info = false
  if self._entity then render_info = self._entity:get_component('render_info') end
  for _, entity in ipairs(self._sv.attached_entities) do
    if render_info then render_info:remove_entity(entity:get_uri()) end
    radiant.entities.destroy_entity(entity)
  end
end

--Ghost form component is often added programmatically and does not exist while SkinChanger create() is called
--Because of that we need to call the necessary function on SkinChanger component directly from ghost form component's post_activate()
function SkinChangerComponent:_on_ghost_form_post_activated()
  if radiant.is_server then
    --[[--
    if self._removed_from_world_listener then
      self._removed_from_world_listener:destroy()
      self._removed_from_world_listener = nil
    end
    --]]--
    if not self._ghost_listener then
      self._ghost_listener = radiant.events.listen_once(self._entity, 'stonehearth:item_placed_on_structure', self, self._on_ghost_placed)
    end
  end
end

function SkinChangerComponent:_on_ghost_placed(event_args)
  -- print('SkinChangerComponent:_on_ghost_placed(): ' .. tostring(self._entity))
  -- print(event_args)
  if event_args.placed_item then
    local skin_changer = event_args.placed_item:get_component('lostems:skin_changer')
    local skin_to_apply = (event_args.options and event_args.options.mod_options and event_args.options.mod_options.skin_name) or self:get_current_skin()
    if skin_changer then
      skin_changer:apply_skin(skin_to_apply, true)
    end
  end
end

function SkinChangerComponent:has_skin(skin_name)
  if skin_name then
    return self._skin_info.skins[skin_name] ~= nil
  end
  return false
end

function SkinChangerComponent:get_default_skin()
  return self._skin_info.default_skin
end

function SkinChangerComponent:get_current_skin()
  return self._sv.current_skin
end

-- note: force_refresh should be used ONLY when applying skin for the first time (i.e. in post_activate() call)
function SkinChangerComponent:apply_skin(skin_name, fallback_to_default, force_refresh)
  -- print('SkinChangerComponent:apply_skin(' .. tostring(skin_name) .. '): ' .. tostring(self._entity))
  if not self._created then
    self._create_skin = skin_name
    return skin_name
  end

  if not force_refresh and skin_name == self:get_current_skin() then
    return skin_name
  end
  skin_name = fallback_to_default and self:validate_skin(skin_name) or (self:has_skin(skin_name) and skin_name)
  if skin_name then
    local render_info = self._entity:get_component('render_info')
    if render_info then
      for _, entity in ipairs(self._sv.attached_entities) do
        render_info:remove_entity(entity:get_uri())
        radiant.entities.destroy_entity(entity)
      end
      if self._skin_info.skins[skin_name].attached_entities then
        for _, uri in ipairs(self._skin_info.skins[skin_name].attached_entities) do
          local entity = radiant.entities.create_entity(uri)
          table.insert(self._sv.attached_entities, entity)
          render_info:attach_entity(entity)
        end
      else
        render_info:set_model_variant(render_info:get_model_variant())
      end
    end
    self._sv.current_skin = skin_name
    self.__saved_variables:mark_changed()
    return skin_name
  end

  return false
end

function SkinChangerComponent:get_next_skin(optional_skin_name)
  -- print('SkinChangerComponent:get_next_skin(' .. tostring(optional_skin_name) .. '): ' .. tostring(self._entity))
  local skins = {}
  for k, _ in pairs(self._skin_info.skins) do table.insert(skins, k) end
  table.sort(skins, function(a,b) return self._skin_info.skins[a].ordinal < self._skin_info.skins[b].ordinal end)
  local curr_id = -1
  local curr_skin = self:has_skin(optional_skin_name) and optional_skin_name or self:get_current_skin()
  for id, v in ipairs(skins) do
    if v == curr_skin then
      curr_id = id
      break
    end
  end
  if curr_id< #skins then
    return skins[curr_id+1]
  elseif curr_id > 0 then
    return skins[1]
  end
  return false
end

function SkinChangerComponent:apply_next_skin(optional_skin_name, force_refresh)
  -- print('SkinChangerComponent:apply_next_skin(' .. tostring(optional_skin_name) .. '): ' .. tostring(self._entity))
  local skin_name = self:get_next_skin(optional_skin_name)
  if skin_name then
    return self:apply_skin(skin_name, nil, force_refresh)
  end
  return false
end

function SkinChangerComponent:apply_default_skin(force_refresh)
  -- print('SkinChangerComponent:apply_default_skin(): ' .. tostring(self._entity))
  if self._skin_info.default_skin then
    return self:apply_skin(self._skin_info.default_skin, nil, force_refresh)
  end
  return false
end

function SkinChangerComponent:validate_skin(skin_name)
  -- print('SkinChangerComponent:validate_skin(' .. tostring(skin_name) .. '): ' .. tostring(self._entity))
  if self:has_skin(skin_name) then
    return skin_name
  end
  return self._skin_info.default_skin
end

return SkinChangerComponent