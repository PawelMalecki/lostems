var debug_itemPaletteShowItemIds = false;
// we're overriding this one completely because the old one would produce an undesireable result anyway and I don't think any other mod changes it

var _oldUpdateItemTooltip = $.stonehearth.stonehearthItemPalette.prototype._updateItemTooltip;
$.stonehearth.stonehearthItemPalette.prototype._updateItemTooltip = function(itemEl, item) {
   if (itemEl.hasClass('tooltipstered')) {
      return;
   }
   var self = this;
   var catalog_data = App.catalog.getCatalogData(item.uri);

   if (catalog_data && catalog_data.skin_changer) {
      $('<div>').addClass('skin-changer-icon').prependTo(itemEl);
   }

   if (typeof stonehearth_ace !== typeof void(0)) {
      _oldUpdateItemTooltip.call(self, itemEl, item);
   }
   else {
      App.tooltipHelper.createDynamicTooltip(itemEl, function() {
         var translationVars = self._geti18nVariables(item);
         var displayNameTranslated = i18n.t(item.display_name, translationVars);
         if (item.deprecated) {
            displayNameTranslated = '<span class="item-tooltip-title item-deprecated">' + displayNameTranslated + '</span>';
         } else if (item.item_quality > 1) {
            displayNameTranslated = '<span class="item-tooltip-title item-quality-' + item.item_quality + '">' + displayNameTranslated + '</span>';
         }

         var description = "";
         var extraTip = "";

         if (item.description) {
            description = i18n.t(item.description, translationVars);
         }

         if (debug_itemPaletteShowItemIds) {
            description = description + '<p>' + self._debugTooltip(item) + '</p>'
         }

         if (item.deprecated) {
            description += '<div class="item-deprecated-tooltip">' + i18n.t('stonehearth:ui.game.entities.tooltip_deprecated') + "</div>";
         }

         if (item.additionalTip) {
            description = description + '<div class="itemAdditionalTip">' + item.additionalTip + "</div>";
         }

         if (catalog_data) {
            if (catalog_data.skin_changer && catalog_data.skin_changer.skins) {
               description += '<div class="item-skin-changer-tooltip">' + i18n.t('lostems:ui.game.item_palette.skin_changer.tip_description') + '</div>';
               Object.keys(catalog_data.skin_changer.skins).sort(function(a, b) {
                  return catalog_data.skin_changer.skins[a].ordinal - catalog_data.skin_changer.skins[b].ordinal;
               }).map((k) => catalog_data.skin_changer.skins[k].icon).forEach(function(icon_path, index) {
                  description += '<img class="item-inside-tooltip-icon" src="' + icon_path + '"/>';
               })
            } else {
               description += '<br/><img class="item-inside-tooltip-icon" src="' + catalog_data.icon + '"/>';
            }

            if (catalog_data.net_worth) {
               var net_worth = radiant.applyItemQualityBonus('net_worth', catalog_data.net_worth, item.item_quality);
               if (net_worth) {  // Ignore zero net_worth
                  extraTip += '<div class="item-net-worth-tooltip">' + net_worth + "</div>";
               }
            }

            // var appeal_data = entity_data !== null ? entity_data['stonehearth:appeal'] : (item.appeal ? { 'appeal': item.appeal } : undefined);
            if (catalog_data.appeal) {
               var appeal = radiant.applyItemQualityBonus('appeal', catalog_data.appeal, item.item_quality);
               if (appeal) {  // Ignore zero appeal
                  extraTip += '<div class="item-appeal-tooltip">' + appeal + "</div>";
               }
            }
         }
         // var entity_data = self._getEntityData(item);
         /* the problem is entity data is always null
         if (entity_data) {
            var combat_info = "";

            var weapon_data = entity_data['stonehearth:combat:weapon_data'];
            if (weapon_data) {
               combat_info = combat_info +
                           '<span id="atkHeader" class="combatHeader">' + i18n.t('stonehearth:ui.game.entities.tooltip_combat_base_damage') + '</span>' +
                           '<span id="atkValue" class="combatValue">+' + weapon_data.base_damage + '</span>';
            }

            var armor_data = entity_data['stonehearth:combat:armor_data'];
            if (armor_data) {
               combat_info = combat_info +
                        '<span id="defHeader" class="combatHeader">' + i18n.t('stonehearth:ui.game.entities.tooltip_combat_base_damage_reduction') + '</span>' +
                        '<span id="defValue" class="combatValue">+' + armor_data.base_damage_reduction + '</span>'
            }

            if (combat_info != "") {
               description = description + '<div class="itemCombatData">' + combat_info + "</div>";
            }
         }
         */

         var tooltip = App.tooltipHelper.createTooltip(displayNameTranslated, description, extraTip);
         return $(tooltip);
      });
   }
}