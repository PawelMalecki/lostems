$(top).on('stonehearthReady', function() {
   // Create a proxy for the workshops object, so we know when a new StonehearthTeamCrafterView is created
   // and can then update it with our own logic.
   App.workshopManager.lostems_workshops = App.workshopManager.workshops;
   App.workshopManager.workshops = new Proxy(App.workshopManager.lostems_workshops, {
      get: function(target, prop) {
         return Reflect.get(target, prop);
      },
      set: function(target, prop, val) {
         App.workshopManager._lostems_updateTeamCrafterView(val);
         return Reflect.set(target, prop, val);
      }
   });

   App.workshopManager._lostems_updateTeamCrafterView = function(teamCrafterView) {
      teamCrafterView.reopen({
         _getOrCalculateRecipeData: function(recipe_key) {
            var result = this._super(...arguments);
            if (result.product_uri) {
               result.lostems_tooltip_data = {}
               var catalog_data = App.catalog.getCatalogData(result.product_uri);
               if (catalog_data) {
                  // result.lostems_tooltip_data.display_name = catalog_data.display_name ? i18n.t(catalog_data.display_name) : '';
                  // result.lostems_tooltip_data.description = catalog_data.description ? i18n.t(catalog_data.description) : '';
                  // result.lostems_tooltip_data.icon = catalog_data.icon ? catalog_data.icon : '';
                  // result.lostems_tooltip_data.net_worth = catalog_data.net_worth ? catalog_data.net_worth : 0;
                  // result.lostems_tooltip_data.appeal = catalog_data.appeal ? catalog_data.appeal : 0;
                  if (catalog_data.skin_changer && catalog_data.skin_changer.skins) {
                     result.lostems_tooltip_data.skin_icons = Object.keys(catalog_data.skin_changer.skins).sort(function(a, b) {
                        return catalog_data.skin_changer.skins[a].ordinal - catalog_data.skin_changer.skins[b].ordinal;
                     }).map((k) => catalog_data.skin_changer.skins[k].icon);
                  }
               }
            }
            return result
         },

         _setPreviewStyling: function() {
            var self = this;
            if (!self.$()) {
               return;  // Menu already closed.
            }
            this._super(...arguments); // Absolutely necessary to call it first because we'll be overriding some already existing tooltips

            this.$('div.item[recipe_key]').each(function() {
               var el = $(this);
               var recipe = self._getOrCalculateRecipeData(el.attr('recipe_key'));
               if (recipe && recipe.lostems_tooltip_data) {
                  if (recipe.lostems_tooltip_data.skin_icons) {
                     $('<div>').addClass('skin-changer-icon').appendTo(el);
                  }

                  if (typeof stonehearth_ace === typeof void(0)) {
                     App.tooltipHelper.createDynamicTooltip(el, function() {
                        // var title = el.attr('title') ? el.attr('title') : recipe.lostems_tooltip_data.display_name;
                        var title = el.attr('title') ? el.attr('title') : i18n.t(recipe.display_name);
                        // var description = recipe.lostems_tooltip_data.description;
                        var description = i18n.t(recipe.description);

                        if (recipe.lostems_tooltip_data.skin_icons) {
                           description += '<div class="item-skin-changer-tooltip">' + i18n.t('lostems:ui.game.item_palette.skin_changer.tip_description') + '</div>';
                           recipe.lostems_tooltip_data.skin_icons.forEach(function (icon_path, index) {
                              description += '<img class="item-inside-tooltip-icon" src="' + icon_path + '"/>';
                           })
                        } else {
                           // description += '<br/><img class="item-inside-tooltip-icon" src="' + recipe.lostems_tooltip_data.icon + '"/>';
                           description += '<br/><img class="item-inside-tooltip-icon" src="' + recipe.portrait + '"/>';
                        }

                        var extraTip = "";
                        /*
                        if (recipe.lostems_tooltip_data.net_worth) {
                           extraTip += '<div class="item-net-worth-tooltip">' + recipe.lostems_tooltip_data.net_worth + "</div>";
                        }

                        if (recipe.lostems_tooltip_data.appeal) {
                           extraTip += '<div class="item-appeal-tooltip">' + recipe.lostems_tooltip_data.appeal + "</div>";
                        }
                        */

                        el.addClass('tooltip_patched_LostEms'); // for other mods to be able to detect altered tooltips
                        var tooltip = App.tooltipHelper.createTooltip(title, description, extraTip);
                        return $(tooltip);
                     });
                  }
               }
            })
         }
      })
   }
})