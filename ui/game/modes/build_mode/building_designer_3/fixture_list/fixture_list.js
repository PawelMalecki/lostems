if (typeof stonehearth_ace !== typeof void(0)) {
   App.StonehearthBuildingFixtureListView.reopen({
      _updateInventoryData: function () {
         var self = this;
         self._super();
         Ember.run.scheduleOnce('afterRender', self, function() {
            self.$('.fixture').each(function() {
               var item_uri = $(this).attr('data-fixture_uri');
               var catalog_data = App.catalog.getCatalogData(item_uri);
               if (catalog_data && catalog_data.skin_changer) {
                  $('<div>').addClass('skin-changer-icon').prependTo($(this));
               }
            });
         });
      },

      actions: {
         addFixture: function(fixtureUri, itemQuality) {
            var catalog_data = App.catalog.getCatalogData(fixtureUri);
            if (catalog_data && catalog_data.skin_changer) {
               if (catalog_data.skin_changer.root_uri && catalog_data.skin_changer.root_uri == fixtureUri) {
                  fixtureUri = catalog_data.skin_changer.skins[0]
               }
            }
            this._super(fixtureUri, itemQuality);
         }
      }
   });
}
else {
   App.StonehearthBuildingFixtureListView.reopen({
      // overriding this one completely because the old one would produce different, unwanted results
      _updateFixtureItemTooltips: function() {
         self.$('.fixture').each(function() {
            var item_uri = $(this).attr('data-fixture_uri');
            var item_quality = $(this).attr('data-item_quality');
            var catalog_data = App.catalog.getCatalogData(item_uri);
            if (catalog_data) {
               if (catalog_data.skin_changer) {
                  $('<div>').addClass('skin-changer-icon').prependTo($(this));
               }
               App.tooltipHelper.createDynamicTooltip($(this), function() {
                  var displayName = i18n.t(catalog_data.display_name);
                  if (item_quality > 1) {
                     displayName = '<span class="item-tooltip-title item-quality-' + item_quality + '">' + displayName + '</span>';
                  }

                  var description = i18n.t(catalog_data.description);

                  if (catalog_data.skin_changer && catalog_data.skin_changer.skins) {
                     description += '<div class="item-skin-changer-tooltip">' + i18n.t('lostems:ui.game.item_palette.skin_changer.tip_description') + '</div>';
                     Object.keys(catalog_data.skin_changer.skins).sort(function(a, b) {
                           return catalog_data.skin_changer.skins[a].ordinal - catalog_data.skin_changer.skins[b].ordinal;
                        }).map((k) => catalog_data.skin_changer.skins[k].icon).forEach(function (icon_path, index) {
                        description += '<img class="item-inside-tooltip-icon" src="' + icon_path + '"/>';
                     })
                  } else {
                     description += '<br/><img class="item-inside-tooltip-icon" src="' + catalog_data.icon + '"/>';
                  }

                  var extraTip = "";

                  if (catalog_data.net_worth) {
                     var net_worth = radiant.applyItemQualityBonus('net_worth', catalog_data.net_worth, item_quality);
                     if (net_worth) {  // Ignore zero net_worth
                        extraTip += '<div class="item-net-worth-tooltip">' + net_worth + "</div>";
                     }
                  }

                  if (catalog_data.appeal) {
                     var appeal = radiant.applyItemQualityBonus('appeal', catalog_data.appeal, item_quality);
                     if (appeal) {  // Ignore zero appeal
                        extraTip += '<div class="item-appeal-tooltip">' + appeal + "</div>";
                     }
                  }

                  var tooltip = App.tooltipHelper.createTooltip(displayName, description, extraTip);
                  return $(tooltip);
               })
            }
         })
      },

      actions: {
         addFixture: function(fixtureUri, itemQuality) {
            var catalog_data = App.catalog.getCatalogData(fixtureUri);
            if (catalog_data && catalog_data.skin_changer) {
               if (catalog_data.skin_changer.root_uri && catalog_data.skin_changer.root_uri == fixtureUri) {
                  fixtureUri = catalog_data.skin_changer.skins[0]
               }
            }
            this._super(fixtureUri, itemQuality);
         }
      }
   });
}