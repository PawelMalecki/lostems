$(document).ready(function() {
   $(top).on('stonehearthReady', function (_, e) {

      var __old_placeItemType = App.stonehearthClient.placeItemType;
      // monkey-patch so we actually never get to place the changer parent entity by type
      App.stonehearthClient.placeItemType = function(itemType, quality) {
        var self = this;
        var catalog_data = App.catalog.getCatalogData(itemType);
        if (catalog_data.skin_changer && catalog_data.skin_changer.root_uri == itemType) {
          itemType = catalog_data.skin_changer.skins[0];
        }
        return __old_placeItemType.call(self, itemType, quality);
      };

      if (typeof stonehearth_ace !== typeof void(0)) {
         var __old_craftAndPlaceItemType = App.stonehearthClient.craftAndPlaceItemType;
         App.stonehearthClient.craftAndPlaceItemType = function(itemType, gameMode) {
            var self = this;
            var catalog_data = App.catalog.getCatalogData(itemType);
            if (catalog_data.skin_changer && catalog_data.skin_changer.root_uri == itemType) {
              itemType = catalog_data.skin_changer.skins[0];
            }
            return __old_craftAndPlaceItemType.call(self, itemType, gameMode);
         };
      }
      else {
         App.stonehearthClient._placeItemOrItemType = function (placementType, toolName, item, quality) {
            var self = this;
            var placementCall = placementType == 'item' ? 'stonehearth:choose_place_item_location' : 'stonehearth:choose_place_item_type_location';
   
            radiant.call('stonehearth:check_can_place_item', item, quality)
               .done(function (response) {
                  self.showTipWithKeyBindings('stonehearth:ui.game.menu.build_menu.items.place_item.tip_title',
                                              'lostems:ui.game.menu.build_menu.items.place_item.tip_description',
                                              {left_binding: 'build:rotate:left', right_binding: 'build:rotate:right', change_skin: 'build:change_skin'});
                  App.setGameMode('place');
                  return self._callTool(toolName, function() {
                     return radiant.call(placementCall, item, quality)
                        .done(function(response) {
                           radiant.call('radiant:play_sound', {'track' : 'stonehearth:sounds:place_structure'} )
                           if ((placementType == 'itemType') && response.more_items) {
                              self.placeItemType(item, quality);
                           } else {
                              self.hideTip();
                           }
                        })
                        .fail(function(response) {
                           //App.setGameMode('normal');
                           self.hideTip();
                        })
                        .always(function(response) {
                           if (placementType == 'item') {
                              App.setGameMode('normal');
                              self.hideTip();
                           }
                        })
                  })
               })
               .fail(function(response) {
                  self.showTip(i18n.t('stonehearth:ui.game.menu.build_menu.items.cannot_place_item.tip_title'),
                               i18n.t('stonehearth:ui.game.menu.build_menu.items.cannot_place_item.tip_description', {
                                  tag: i18n.t("i18n(stonehearth:ui.game.unit_frame.placement_tags." + response.placement_tag + ")"),
                                  num: response.num_placed || 0,
                                  max: response.max_placeable || 0
                               }),
                               {warning: 'warning'});
               })
         }
      }
   })
})
