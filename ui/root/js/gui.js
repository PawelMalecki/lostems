if (typeof stonehearth_ace !== typeof void(0)) {
   var _oldCreateUriTooltip = App.guiHelper.createUriTooltip;
   App.guiHelper.createUriTooltip = function(uri, options) {
      var catalog_data = App.catalog.getCatalogData(uri);
      var moreDetails = '';
      if (catalog_data && catalog_data.skin_changer && catalog_data.skin_changer.skins) {
         moreDetails += '<div class="item-skin-changer-tooltip">' + i18n.t('lostems:ui.game.item_palette.skin_changer.tip_description') + '</div>';
            Object.keys(catalog_data.skin_changer.skins).sort(function(a, b) {
               return catalog_data.skin_changer.skins[a].ordinal - catalog_data.skin_changer.skins[b].ordinal;
            }).map((k) => catalog_data.skin_changer.skins[k].icon).forEach(function(icon_path, index) {
               moreDetails += '<img class="item-inside-tooltip-icon" src="' + icon_path + '"/>';
            })
      } else if (catalog_data && catalog_data.icon) {
         moreDetails += '<br/><img class="item-inside-tooltip-icon" src="' + catalog_data.icon + '"/>';
      }
      if (moreDetails != '') {
         options = options || {};
         options.moreDetails = (options.moreDetails || '') + moreDetails;
      }

      return _oldCreateUriTooltip.call(this, uri, options);
   }
}