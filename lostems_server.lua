lostems = {}

lostems.server_patches = {
   radiant = {
      lostems_place_item_call_handler = 'stonehearth.call_handlers.place_item_call_handler',
      lostems_blueprint = 'stonehearth.components.building2.blueprints.blueprint',
      lostems_entity_forms_component = 'stonehearth.components.entity_forms.entity_forms_component',
      lostems_fixture_data = 'stonehearth.lib.building.fixture_data',
      lostems_template_utils = 'stonehearth.lib.building.template_utils',
      -- ACE-specific patches:
      lostems_fixture = 'stonehearth.components.building2.fixture',
      lostems_ghost_form_component = 'stonehearth.components.ghost_form.ghost_form_component',
      lostems_entity_forms_lib = 'stonehearth.lib.entity_forms.entity_forms_lib',
      lostems_building_service = 'stonehearth.services.server.building.building_service'
   },
   stonehearth_ace = {
      lostems_fixture = 'stonehearth.components.building2.fixture',
      lostems_ghost_form_component = 'stonehearth.components.ghost_form.ghost_form_component',
      lostems_entity_forms_lib = 'stonehearth.lib.entity_forms.entity_forms_lib',
      lostems_building_service = 'stonehearth.services.server.building.building_service',
      lostems_ace_town_call_handler = 'stonehearth_ace.call_handlers.town_call_handler'
   }
}

function lostems.is_mod_loaded(namespace)
   for _, mod_name in ipairs(radiant.resources.get_mod_list()) do
      if mod_name == namespace then
         return true
      end
   end
   return false
end

function lostems.monkey_patching(patch_list)
   for from, into in pairs(patch_list) do
      local monkey_see = require('monkey_patches.' .. from)
      local monkey_do = radiant.mods.require(into)
      radiant.log.write_('lostems', 0, 'LostEms server monkey-patching sources \'' .. from .. '\' => \'' .. into .. '\'')
      if monkey_see.MERGE_PATCH_INTO_TABLE then
         -- use merge_into_table to also mixin other values, not just functions
         radiant.util.merge_into_table(monkey_do, monkey_see)
      else
         radiant.mixin(monkey_do, monkey_see)
      end
   end
end

function lostems._on_init()
   radiant.log.write_('lostems', 0, 'LostEms (v4.2.1) server initialized')
   radiant.events.trigger_async(radiant, 'lostems:server:init')
end

for k,v in pairs(lostems.server_patches) do
   if k ~= 'radiant' then
      if lostems.is_mod_loaded(k) then
         for from, into in pairs(v) do
            lostems.server_patches.radiant[from] = nil
         end
      else
         lostems.server_patches[k] = nil
      end
   end
end

radiant.events.listen(lostems, 'radiant:init', lostems, lostems._on_init)
for k,v in pairs(lostems.server_patches) do
   radiant.events.listen(radiant, k .. (k == 'radiant' and '' or ':server') .. ':required_loaded', function()
      lostems.monkey_patching(v)
      if k == 'radiant' then
         radiant.events.trigger(radiant, 'lostems:server:required_loaded')
         radiant.log.write_('lostems', 0, 'LostEms: required loaded')
      else
         radiant.events.trigger(radiant, 'lostems:server:required_loaded:' .. k)
         radiant.log.write_('lostems', 0, 'LostEms: required loaded after ' .. k)
      end
   end)
end

return lostems
