lostems = {}

lostems.client_patches = {
   radiant = {
      lostems_blueprint = 'stonehearth.components.building2.blueprints.blueprint',
      lostems_temp_building = 'stonehearth.components.building2.temp_building',
      lostems_entity_forms_component = 'stonehearth.components.entity_forms.entity_forms_component',
      lostems_fixture_data = 'stonehearth.lib.building.fixture_data',
      lostems_template_utils = 'stonehearth.lib.building.template_utils',
      lostems_building_client_service = 'stonehearth.services.client.building.building_client_service',
      lostems_decoration_tool = 'stonehearth.services.client.building.decoration_tool',
      lostems_fixture_widget = 'stonehearth.services.client.widget.fixture_widget',
      -- ACE-specific patches:
      lostems_fixture = 'stonehearth.components.building2.fixture',
      lostems_ghost_form_component = 'stonehearth.components.ghost_form.ghost_form_component',
      lostems_entity_forms_lib = 'stonehearth.lib.entity_forms.entity_forms_lib',
      lostems_item_placer = 'stonehearth.services.client.build_editor.item_placer',
      lostems_entity_or_location_selector = 'stonehearth.services.client.selection.entity_or_location_selector'
   },
   stonehearth_ace = {
      lostems_fixture = 'stonehearth.components.building2.fixture',
      lostems_ghost_form_component = 'stonehearth.components.ghost_form.ghost_form_component',
      lostems_entity_forms_lib = 'stonehearth.lib.entity_forms.entity_forms_lib',
      lostems_item_placer = 'stonehearth.services.client.build_editor.item_placer',
      lostems_entity_or_location_selector = 'stonehearth.services.client.selection.entity_or_location_selector'
   }
}

function lostems.is_mod_loaded(namespace)
   for _, mod in ipairs(radiant.resources.get_mod_list()) do
      if mod == namespace then
         return true
      end
   end
   return false
end

function lostems.monkey_patching(patch_list)
   for from, into in pairs(patch_list) do
      local monkey_see = require('monkey_patches.' .. from)
      local monkey_do = radiant.mods.require(into)
      radiant.log.write_('lostems', 0, 'LostEms client monkey-patching sources \'' .. from .. '\' => \'' .. into .. '\'')
      if monkey_see.MERGE_PATCH_INTO_TABLE then
         -- use merge_into_table to also mixin other values, not just functions
         radiant.util.merge_into_table(monkey_do, monkey_see)
      else
         radiant.mixin(monkey_do, monkey_see)
      end
   end
end

function lostems._patch_catalog_client()
   if stonehearth.catalog_client then
      radiant.log.write_('lostems', 0, 'LostEms: Client catalog patching started.')
      for alias, _ in pairs(stonehearth.catalog_client._sv.catalog) do
         local skin_changer = radiant.entities.get_entity_data(alias, 'lostems:skin_info')
         if skin_changer and skin_changer.skins then
            stonehearth.catalog_client._sv.catalog[alias].skin_changer = {
               default_skin = skin_changer.default_skin,
               skins = {}
            }
            for k, v in pairs(skin_changer.skins) do
               stonehearth.catalog_client._sv.catalog[alias].skin_changer.skins[k] = {
                  icon = v.icon,
                  ordinal = v.ordinal
               }
            end
         end
      end
      radiant.log.write_('lostems', 0, 'LostEms: Client catalog patching finished.')
   else
      radiant.log.write_('lostems', 0, 'LostEms ERROR: client catalog not found!')
   end
end

function lostems._on_init()
   radiant.log.write_('lostems', 0, 'LostEms (v4.2.1) client initialized')
   lostems._patch_catalog_client()
   radiant.events.trigger_async(radiant, 'lostems:client:init')
end

for k,v in pairs(lostems.client_patches) do
   if k ~= 'radiant' then
      if lostems.is_mod_loaded(k) then
         for from,into in pairs(v) do
            lostems.client_patches.radiant[from] = nil
         end
      else
         lostems.client_patches[k] = nil
      end
   end
end

radiant.events.listen(lostems, 'radiant:init', lostems, lostems._on_init)
for k,v in pairs(lostems.client_patches) do
   radiant.events.listen(radiant, k .. (k == 'radiant' and '' or ':client') .. ':required_loaded', function()
      lostems.monkey_patching(v)
      if k == 'radiant' then
         radiant.events.trigger(radiant, 'lostems:client:required_loaded')
         radiant.log.write_('lostems', 0, 'LostEms: required loaded')
      else
         radiant.events.trigger(radiant, 'lostems:client:required_loaded:' .. k)
         radiant.log.write_('lostems', 0, 'LostEms: required loaded after ' .. k)
      end
   end)
end

return lostems