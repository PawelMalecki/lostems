local SkinChangerCallHandler = class()

function SkinChangerCallHandler:get_current_skin(session, response, entity)
   local skin_changer = entity:get_component('lostems:skin_changer')
   if skin_changer then
      return { current_skin = skin_changer:get_current_skin() }
   end
   return { current_skin = false }
end

function SkinChangerCallHandler:apply_skin(session, response, entity, skin_name)
   local skin_changer = entity:get_component('lostems:skin_changer')
   if skin_changer then
      local result = skin_changer:apply_skin(skin_name, false)
      return { success = (result ~= nil and result ~= false) }
   end
   return { success = false }
end

function SkinChangerCallHandler:apply_next_skin(session, response, entity, optional_skin_name)
   local skin_changer = entity:get_component('lostems:skin_changer')
   if skin_changer then
      local result = skin_changer:apply_next_skin(optional_skin_name)
      return { success = (result ~= nil and result ~= false) }
   end
   return { success = false }
end

return SkinChangerCallHandler