local build_util = require 'stonehearth.lib.build_util'
local entity_forms = require 'stonehearth.lib.entity_forms.entity_forms_lib'
-- local ItemPlacer = require 'stonehearth.services.client.build_editor.item_placer'
local Entity = _radiant.om.Entity
local Point3 = _radiant.csg.Point3
local validator = radiant.validator

local PlaceItemCallHandler = require 'stonehearth.call_handlers.place_item_call_handler'
local LostEmsPlaceItemCallHandler = class()
local log = radiant.log.create_logger('call_handlers.place_item')

local function get_root_entity(item)
   if not item or not radiant.util.is_a(item, Entity) or not item:is_valid() then
      return
   end

   local iconic_form = item:get_component('stonehearth:iconic_form')
   if iconic_form then
      item = iconic_form:get_root_entity()
   end
   local ghost_form = item:get_component('stonehearth:ghost_form')
   if ghost_form then
      item = ghost_form:get_root_entity()
   end

   local entity_forms = item:get_component('stonehearth:entity_forms')
   if entity_forms then
      return item, entity_forms
   end
end

--- Tell a worker to place the item in the world
function LostEmsPlaceItemCallHandler:place_item_in_world(session, response, item_to_place, location, rotation, normal, mod_options)
   validator.expect_argument_types({'Entity', 'Point3', 'number', 'Point3'}, item_to_place, location, rotation, normal) --any type for table or userdata

   location = radiant.util.to_point3(location)
   normal = radiant.util.to_point3(normal)

   local item, entity_forms = get_root_entity(item_to_place)
   if not entity_forms then
      response:reject({ error = 'item has no entity_forms component'})
      return
   end

   radiant.entities.set_player_id(item, session.player_id)
   entity_forms:place_item_on_ground(location, rotation, normal, mod_options)

   return true
end

--- Tell a worker to place the item in the world
function LostEmsPlaceItemCallHandler:place_item_on_structure(session, response, item, world_location, rotation, structure_entity, normal, transactional, mod_options)
   validator.expect_argument_types({'Entity', 'Point3', 'number', 'Entity', 'Point3', 'boolean'}, item, world_location, rotation, structure_entity, normal, transactional)

   world_location = radiant.util.to_point3(world_location)
   local p3_normal = radiant.util.to_point3(normal)

   local item, entity_forms = get_root_entity(item)
   if not entity_forms then
      response:reject({ error = 'item has no entity_forms component'})
   end

   if structure_entity:get_uri() == 'stonehearth:build2:entities:structure' then
      return self:_place_item_on_new_structure(session, response, item, world_location, rotation, structure_entity, p3_normal, mod_options)
   end

   local location = world_location - radiant.entities.get_world_grid_location(structure_entity)

   -- If object is placed on a done building or a non-building structure, then we need to tell the entity
   -- forms to place it and create the task.
   local building = build_util.get_building_for(structure_entity)
   if not building or building:get_component('stonehearth:construction_progress'):get_finished() then
      radiant.entities.set_player_id(item, session.player_id)
      entity_forms:place_item_on_structure(world_location, structure_entity, rotation, p3_normal, mod_options)
   end

   local ghost = entity_forms:get_ghost_placement_entity()
   if transactional then
      stonehearth.build:add_fixture_command(session, response, structure_entity, item, nil, location, normal, rotation, ghost, mod_options)
   else
      stonehearth.build:add_fixture(structure_entity, item, nil, location, p3_normal, rotation, ghost, mod_options)
   end

   return true
end

--- Place any object that matches the entity_uri
-- server side object to handle creation of the workbench.  this is called
-- by doing a POST to the route for this file specified in the manifest.
function LostEmsPlaceItemCallHandler:place_item_type_in_world(session, response, entity_uri, quality, location, rotation, normal, mod_options)
   validator.expect_argument_types({'string', validator.optional('number'), 'Point3', 'number', 'Point3'}, entity_uri, quality, location, rotation, normal)

   location = radiant.util.to_point3(location)
   normal = radiant.util.to_point3(normal)

   -- look for entities which are not currently being placed
   local town = stonehearth.town:get_town(session.player_id)
   if not town then
      return response:reject({error = 'no town for player'})
   end
   local placement_info = {
         location = location,
         normal = normal,
         rotation = rotation,
         structure = radiant._root_entity,
         preserve_destination = true,
         mod_options = mod_options
      }
   town:place_item_type(entity_uri, quality, placement_info)

   local more_items = self:_can_place_more(session.player_id, entity_uri, quality)
   -- return whether or not there are most items we could potentially place
   response:resolve({
      more_items = more_items,
      item_uri = entity_uri
   })
end

function LostEmsPlaceItemCallHandler:_place_item_on_new_structure(session, response, item, world_location, rotation, structure_entity, normal, mod_options)
   local _, entity_forms = get_root_entity(item)
   radiant.entities.set_player_id(item, session.player_id)
   entity_forms:place_item_on_structure(world_location, structure_entity, rotation, normal, mod_options)
   return true
end

function LostEmsPlaceItemCallHandler:_place_item_type_on_new_structure(session, response, entity_uri, quality, location, rotation, structure_entity, normal, mod_options)
   -- look for entities which are not currently being placed
   local town = stonehearth.town:get_town(session.player_id)
   if not town then
      return response:reject({error = 'no town for player'})
   end
   local placement_info = {
         location = location,
         normal = normal,
         rotation = rotation,
         structure = structure_entity,
         preserve_destination = true,
         mod_options = mod_options
      }
   town:place_item_type(entity_uri, quality, placement_info)

   local more_items = self:_can_place_more(session.player_id, entity_uri, quality)
   -- return whether or not there are most items we could potentially place
   response:resolve({
      more_items = more_items,
      item_uri = entity_uri
   })
end

--- Place any object that matches the entity_uri
-- server side object to handle creation of the workbench.  this is called
-- by doing a POST to the route for this file specified in the manifest.
-- "quality" can be a number or nil for to allow any quality.
function LostEmsPlaceItemCallHandler:place_item_type_on_structure(session, response, entity_uri, quality, location, rotation, structure_entity, normal, transactional, mod_options)
   validator.expect_argument_types({'string', validator.optional('number'), 'Point3', 'number', 'Entity', 'Point3', 'boolean'}, entity_uri, quality, location, rotation, structure_entity, normal, transactional)

   local world_location = Point3(location.x, location.y, location.z)
   local p3_normal = radiant.util.to_point3(normal)

   local fixture_uri = entity_uri
   local data = radiant.entities.get_component_data(fixture_uri, 'stonehearth:entity_forms')
   assert(data, 'must send entity-forms root entity to place_item_type_on_structure')
   local p3_location = world_location - radiant.entities.get_world_grid_location(structure_entity)
   location = { x = p3_location.x, y = p3_location.y, z = p3_location.z }
   
   if not build_util.is_blueprint(structure_entity) then
      return self:_place_item_type_on_new_structure(session, response, entity_uri, quality, world_location, rotation, structure_entity, p3_normal, mod_options)
   else
      if transactional then
         stonehearth.build:add_fixture_command(session, response, structure_entity, fixture_uri, quality, location, normal, rotation, mod_options)
      else
         stonehearth.build:add_fixture(structure_entity, fixture_uri, quality, p3_location, p3_normal, rotation, mod_options)
      end
   end

   local more_items = self:_can_place_more(session.player_id, entity_uri, quality)
   -- return whether or not there are most items we could potentially place
   response:resolve({
      more_items = more_items,
      item_uri = entity_uri
   })
end

return LostEmsPlaceItemCallHandler
