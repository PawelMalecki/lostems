-- local Cube3 = _radiant.csg.Cube3
-- local Region3 = _radiant.csg.Region3
local Point3 = _radiant.csg.Point3
-- local Point2 = _radiant.csg.Point2
local mutation_utils = require 'stonehearth.lib.building.mutation_utils'
-- local template_utils = require 'stonehearth.lib.building.template_utils'
-- local entity_forms = require 'stonehearth.lib.entity_forms.entity_forms_lib'
-- local WallData = require 'stonehearth.lib.building.wall_data'
-- local RoofData = require 'stonehearth.lib.building.roof_data'
-- local RoomData = require 'stonehearth.lib.building.room_data'
local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- local BlocksData = require 'stonehearth.lib.building.blocks_data'
-- local StairsData = require 'stonehearth.lib.building.stairs_data'
-- local RoadData = require 'stonehearth.lib.building.road_data'
-- local BuildingDestructionJob = require 'stonehearth.components.building2.building_destruction_job'
-- local validator = radiant.validator

-- local log = radiant.log.create_logger('building_service')

-- local NUMBERED_BUILDING_DISPLAY_NAME = 'i18n(stonehearth:data.build.prototypes.building.numbered_display_name)'
-- local Building = 'stonehearth:build2:building'

local BuildingService = require 'stonehearth.services.server.building.building_service'
local LostEmsBuildingService = class()
--[[
local function _fix_bids(bad_bids)
   local bids = {}
   for b, _ in pairs(bad_bids) do
      bids[tonumber(b)] = true
   end
   return bids
end
]]--

--LostEmsBuildingService._old_lostems_add_fixture_command = BuildingService.add_fixture_command
function LostEmsBuildingService:add_fixture_command(building_id, owner_bid, uri, quality, origin, direction, owner_sub_bid, rotation, mod_options)
   origin = Point3(origin.x, origin.y, origin.z)
   direction = Point3(direction.x, direction.y, direction.z)
   rotation = rotation or 0
   mod_options = mod_options or {}
   -- print('BuildingService:add_fixture_command(): ' .. uri)
   -- print(mod_options)
   local bid = self:get_next_bid()

   local fixture_data = FixtureData.Make(building_id, bid, uri, quality, owner_bid, origin, direction, owner_sub_bid, rotation, mod_options)
   mutation_utils.create(fixture_data)

   return bid
end

return LostEmsBuildingService
