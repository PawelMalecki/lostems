local FixtureData = require 'stonehearth.lib.building.fixture_data'
local Point3 = _radiant.csg.Point3
-- local Region3 = _radiant.csg.Region3
local Cube3   = _radiant.csg.Cube3
-- local Color4   = _radiant.csg.Color4

-- local log = radiant.log.create_logger('build.temp_building')

local TempBuilding = require 'stonehearth.components.building2.temp_building'
local LostEmsTempBuilding = class()

--ALL GODS THIS MONSTER SHOULD BE SPLIT INTO PARTS SO PEOPLE CAN ACTUALLY PATCH IT!
--compare with template_utils.save_building_template_screenshot()
function LostEmsTempBuilding:finish()
   for _, data in pairs(self._data) do
      data:deferred_build()
   end

   -- Figure out what the 'bottom' of the building is, and offset everyone by that amount.
   local min_y = 999999999
   local bounds
   for _, data in pairs(self._data) do
      local data_bounds = data:get_world_real_region():get_bounds()
      if data_bounds:get_area() > 0 then
         min_y = math.min(data_bounds.min.y, min_y)
         if not bounds then
            bounds = data_bounds
         else
            bounds:grow(data_bounds)
         end
      end
   end

   if not bounds then
      bounds = Cube3(Point3(0, 0, 0), Point3(1, 1, 1))
   end
   local mid = (bounds.max + bounds.min) / 2
   self._offset = Point3(-mid.x, -min_y, -mid.z):to_int()

   for _, data in pairs(self._data) do
      if data:get_uri() == FixtureData.URI then
         --Pawel: this entity should be created by a separate function taking self and data as arguments so it's possible to mod the constructor function instead of overriding the whole finish() function
         --local f = self:_create_fixture_entity_from_data(data)
         local f = radiant.entities.create_entity(data:get_fixture_uri(), {
            owner = self._entity
            })

         radiant.terrain.place_entity_at_exact_location(f, data:get_world_origin() + self._offset , {
               root_entity = self._entity,
               force_iconic = false,
            })
         radiant.entities.turn_to(f, data:get_rotation())

         ----[[-- LostEms code here:
         local mod_options = (data.get_mod_options ~= nil and data:get_mod_options() or data.mod_options)
         if mod_options and mod_options.skin_name then
            local skin_changer = f:get_component('lostems:skin_changer')
            if skin_changer then skin_changer:apply_skin(mod_options.skin_name) end
         end
         --]]--
      else
         --Pawel: this could be refactored into a separate function like it is done in template_utils.save_building_template_screenshot()
         local structure = radiant.entities.create_entity('stonehearth:build2:entities:temp_structure', { owner = self._entity })
         radiant.terrain.place_entity_at_exact_location(structure, self._offset, {root_entity = self._entity})
         local wr = data:get_world_real_region()
         self._region:add_region(wr:translated(self._offset))
         structure:get('region_collision_shape'):get_region():modify(function(cursor)
               cursor:copy_region(wr)
               cursor:set_tag(0)
               cursor:optimize('temp building')
            end)
         structure:get('destination'):get_region():modify(function(cursor)
               cursor:copy_region(wr)
            end)
         -- TODO: ugh, fix this when we get proper ownership.
         if data.get_wall_map then
            for _, walls in data:get_wall_map():each() do
               for _, wall in pairs(walls) do
                  local structure = radiant.entities.create_entity('stonehearth:build2:entities:temp_structure', { owner = self._entity })
                  local wr = wall:get_world_real_region()
                  self._region:add_region(wr:translated(self._offset))
                  radiant.terrain.place_entity_at_exact_location(structure, self._offset, {root_entity = self._entity})
                  structure:get('region_collision_shape'):get_region():modify(function(cursor)
                        cursor:copy_region(wr)
                        cursor:set_tag(0)
                        cursor:optimize('temp building')
                     end)
                  structure:get('destination'):get_region():modify(function(cursor)
                        cursor:copy_region(wr)
                     end)
               end
            end
         end
      end
   end

   for _, data in pairs(self._data) do
      stonehearth.building:remove_data(data)
   end

   self._bounds = self._region:get_bounds()
   self._data = {}
end

--[[--
function LostEmsTempBuilding:_create_fixture_entity_from_data(data)
   local f = radiant.entities.create_entity(data:get_fixture_uri(), {
      owner = self._entity
      })

   radiant.terrain.place_entity_at_exact_location(f, data:get_world_origin() + self._offset , {
         root_entity = self._entity,
         force_iconic = false,
      })
   radiant.entities.turn_to(f, data:get_rotation())

   -- LostEms code here:
   local mod_options = (data.get_mod_options ~= nil and data:get_mod_options() or data.mod_options)
   if mod_options and mod_options.skin_name then
      local skin_changer = f:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(mod_options.skin_name) end
   end

   return f
end
--]]--

return LostEmsTempBuilding
