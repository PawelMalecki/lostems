-- local SelectorBase = require 'stonehearth.services.client.selection.selector_base'
-- local selector_util = require 'stonehearth.services.client.selection.selector_util'
-- local Point3 = _radiant.csg.Point3
local bindings = _radiant.client.get_binding_system()
-- local Entity = _radiant.om.Entity
local EntityOrLocationSelector = require 'stonehearth.services.client.selection.entity_or_location_selector'

local LostEmsEntityOrLocationSelector = class()
-- local FootprintWidget = require 'stonehearth.services.client.selection.footprint_widget'

-- radiant.mixin(EntityOrLocationSelector, SelectorBase)

-- local OFFSCREEN = Point3(0, -100000, 0)
-- local INVALID_CURSOR = 'stonehearth:cursors:invalid_hover'

LostEmsEntityOrLocationSelector._old_lostems_on_keyboard_event = EntityOrLocationSelector._on_keyboard_event
-- handles keyboard events from the input service
-- Use comma and period to rotate the item
function LostEmsEntityOrLocationSelector:_on_keyboard_event(e)
   local event_consumed = false

   -- print('EntityOrLocationSelector:_on_keyboard_event(): skin_changing = ' .. tostring(self._skin_changing_enabled))
   if self._skin_changing_enabled == true and bindings:is_action_active('build:change_skin') then
      event_consumed = self:_set_next_skin()
   end
   
   if event_consumed then
      return true
   end
   return self:_old_lostems_on_keyboard_event(e)
end

function LostEmsEntityOrLocationSelector:_get_mod_options()
   return self._mod_options or {}
end

LostEmsEntityOrLocationSelector._old_lostems_set_cursor_entity = EntityOrLocationSelector.set_cursor_entity
function LostEmsEntityOrLocationSelector:set_cursor_entity(cursor_entity)
   -- print('EntityOrLocationSelector:set_cursor_entity()' .. tostring(cursor_entity))
   self:_old_lostems_set_cursor_entity(cursor_entity)
   self._mod_options = self._mod_options or {}
   local skin_changer = self._cursor_entity:get_component('lostems:skin_changer')
   if skin_changer then
      self:_unlock_skin_changing()
      if self._mod_options.skin_name == nil then
         self._mod_options.skin_name = skin_changer:apply_default_skin()
      else
         self._mod_options.skin_name = skin_changer:apply_skin(self._mod_options.skin_name, true)
      end
   else
      self:_lock_skin_changing()
   end
   -- print(self._mod_options)
   return self
end


function LostEmsEntityOrLocationSelector:_set_next_skin()
   -- print('LostEmsEntityOrLocationSelector:_set_next_skin(): ' .. tostring(self._cursor_entity))
   self._mod_options = self._mod_options or {}
   if self._cursor_entity then
      local skin_changer = self._cursor_entity:get_component('lostems:skin_changer')
      if skin_changer then
         self._mod_options.skin_name = skin_changer:apply_next_skin()
         -- print('skin_name = ' .. tostring(self._mod_options.skin_name))
         self:_on_mouse_event(_radiant.client.get_mouse_position())
         return true
      end
   end
   return false
end

function LostEmsEntityOrLocationSelector:_unlock_skin_changing()
   -- print('LostEmsEntityOrLocationSelector:_unlock_skin_changing()')
   self._skin_changing_enabled = true
end

function LostEmsEntityOrLocationSelector:_lock_skin_changing()
   -- print('LostEmsEntityOrLocationSelector:_lock_skin_changing()')
   self._skin_changing_enabled = false
end

return LostEmsEntityOrLocationSelector
