-- local Point3 = _radiant.csg.Point3
-- local Region3 = _radiant.csg.Region3

-- local BlueprintFixture = require 'stonehearth.components.building2.blueprints.blueprint_fixture'
-- local BlueprintStructure = require 'stonehearth.components.building2.blueprints.blueprint_structure'

local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- local RoomData = require 'stonehearth.lib.building.room_data'
-- local RoofData = require 'stonehearth.lib.building.roof_data'
-- local WallData = require 'stonehearth.lib.building.wall_data'
-- local RoadData = require 'stonehearth.lib.building.road_data'
-- local BlocksData = require 'stonehearth.lib.building.blocks_data'

-- local fixture_utils = require 'stonehearth.lib.building.fixture_utils'
-- local log = radiant.log.create_logger('build.blueprint')

local Blueprint = require 'stonehearth.components.building2.blueprints.blueprint'
local LostEmsBlueprint = class()

LostEmsBlueprint._old_lostems_diff = Blueprint.diff
function LostEmsBlueprint:diff(added_structures, removed_structures, mutated_structures, removed_voxels_ws, added_fixtures)
   -- print('Blueprint:diff()')
   local result = self:_old_lostems_diff(added_structures, removed_structures, mutated_structures, removed_voxels_ws, added_fixtures)
   if self:get_data():get_uri() == FixtureData.URI then
      local mod_options = (self:get_data().get_mod_options ~= nil) and self:get_data():get_mod_options() or self:get_data().mod_options
      -- print(mod_options)
      if mod_options then
         added_fixtures[self:get_bid()]._placement_info.mod_options = mod_options
      end
   end
   return result
end

--[[--
LostEmsBlueprint._old_lostems_update_data = Blueprint.update_data
function LostEmsBlueprint:update_data(data)
   local result = self:_old_lostems_update_data(data)

   if data:get_uri() == FixtureData.URI then
      local mod_options = (data.get_mod_options ~= nil) and data:get_mod_options() or data.mod_options
      print('LostEmsBlueprint:update_data() called. mod_options:')
      print(mod_options)
   end

   return result
end
--]]--

return LostEmsBlueprint
