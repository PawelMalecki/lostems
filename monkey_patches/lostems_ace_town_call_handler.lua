local validator = radiant.validator
local TownCallHandler = require 'stonehearth_ace.call_handlers.town_call_handler'
local LostEmsTownCallHandler = class()

--LostEmsTownCallHandler._old_lostems_craft_and_place_item_type_in_world = TownCallHandler.craft_and_place_item_type_in_world
function LostEmsTownCallHandler:craft_and_place_item_type_in_world(session, response, uri, location, rotation, normal, structure, mod_options)
   validator.expect_argument_types({'string', 'Point3', 'number', 'Point3', validator.optional('Entity'), validator.optional('table')},
      uri, location, rotation, normal, structure, mod_options)

   local town = stonehearth.town:get_town(session.player_id)
   if town then
      location = radiant.util.to_point3(location)
      normal = radiant.util.to_point3(normal)

      local placement_info = {
         location = location,
         normal = normal,
         rotation = rotation,
         structure = structure or radiant._root_entity,
         preserve_destination = true,
         mod_options = mod_options
      }
      town:craft_and_place_item_type(uri, placement_info)

      response:resolve({
         item_uri = uri,
         more_items = true,
      })
   else
      response:reject({error = 'no town for player'})
   end
end

return LostEmsTownCallHandler
