local EntityFormsLib = require 'stonehearth.lib.entity_forms.entity_forms_lib'

local SkinSwapperEntityFormsLib = class()

-- local Entity = _radiant.om.Entity

SkinSwapperEntityFormsLib._old_lostems_initialize_ghost_form_components = EntityFormsLib.initialize_ghost_form_components
function SkinSwapperEntityFormsLib.initialize_ghost_form_components(ghost, root_uri, quality, json, placement_info)
   local result = SkinSwapperEntityFormsLib._old_lostems_initialize_ghost_form_components(ghost, root_uri, quality, json, placement_info)
   if placement_info and placement_info.mod_options and placement_info.mod_options.skin_name then
      local skin_changer = ghost:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(placement_info.mod_options.skin_name) end
   end
   return result
end

return SkinSwapperEntityFormsLib
