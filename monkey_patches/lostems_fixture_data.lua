-- local Region2 = _radiant.csg.Region2
local Region3 = _radiant.csg.Region3
-- local Cube3 = _radiant.csg.Cube3
local Point3 = _radiant.csg.Point3

-- local log = radiant.log.create_logger('build.fixture_data')

local BuildingData = require 'stonehearth.lib.building.building_data'
local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- FixtureData.URI = 'stonehearth:build2:entities:fixture_blueprint'

local fixture_utils = require 'stonehearth.lib.building.fixture_utils'
-- local constants = require 'stonehearth.constants'

-- Returns a rotation value used to rotate fixture clockwise relative to the
-- initial facing of the camera.
local function _get_new_cw_rotation(rotation)
  return (rotation - 90) % 360
end

-- Returns a direction to point fixture clockwise relative to facing of the initial camera pos.
local function _get_new_cw_direction(direction)
  local new_direction = direction

  if new_direction.x < 0 then
    new_direction.x = 0
    new_direction.z = -1
  elseif new_direction.x > 0 then
    new_direction.x = 0
    new_direction.z = 1
  elseif new_direction.z < 0 then
    new_direction.z = 0
    new_direction.x = 1
  elseif new_direction.z > 0 then
    new_direction.z = 0
    new_direction.x = -1
  end

  return new_direction
end

local LostEmsFixtureData = radiant.class()

function LostEmsFixtureData.Make(building_id, bid, uri, quality, owner_bid, origin, direction, sub_data_bid, rotation, mod_options)
  -- print('LostEmsFixtureData.Make(): ' .. uri)
  -- print(mod_options)
  return FixtureData(building_id, bid, uri, quality, owner_bid, origin, direction, nil, sub_data_bid, rotation, mod_options)
end

LostEmsFixtureData._old_lostems__init = FixtureData.__init
function LostEmsFixtureData:__init(building_id, bid, uri, quality, owner_bid, origin, direction, masked, sub_data_bid, rotation, mod_options)
  local result = self:_old_lostems__init(building_id, bid, uri, quality, owner_bid, origin, direction, masked, sub_data_bid, rotation)
  self.mod_options = mod_options or {}
  return result
end

LostEmsFixtureData._old_lostems_dangerous_rotate = FixtureData.dangerous_rotate
function LostEmsFixtureData:dangerous_rotate(point_w, degrees)
  local num_loops = degrees / 90
  local new_rotation = self.rotation
  local new_direction = self.direction
  local new_origin = self:get_world_origin()

  local offset = self.origin - self:get_world_origin()
  for i=1,num_loops do
    new_rotation = _get_new_cw_rotation(new_rotation)
    new_direction = _get_new_cw_direction(new_direction)
  end

  local origin_fixup = self:_get_origin_fixup(new_rotation)

  new_origin = (((new_origin - point_w):rotated(360 - degrees)))
  new_origin = new_origin + point_w + origin_fixup

  new_origin = (new_origin + offset)

  self:__init(
    self.building_id,
    self.bid,
    self.uri,
    self.quality,
    self.owner_bid,
    new_origin,
    new_direction,
    self.masked,
    self.sub_data_bid,
    new_rotation,
    self.mod_options)
end

function LostEmsFixtureData:get_mod_options()
  return self.mod_options
end

function LostEmsFixtureData:set_mod_options(mod_options)
  return self:_new({mod_options = mod_options})
end

function LostEmsFixtureData:equals(other)
  if not self:_equals(other) then
    return false
  end

  return self.owner_bid == other.owner_bid and
  self.sub_data_bid == other.sub_data_bid and
  self.direction == other.direction and
  self.rotation == other.rotation and
  self.quality == other.quality and
  table.concat(self.mod_options) == table.concat(other.mod_options)
end

function LostEmsFixtureData:_new(params)
  local origin = params.origin or self.origin
  local masked = params.masked or self.masked
  local direction = params.direction or self.direction
  local owner_bid = params.owner_bid or self.owner_bid
  local quality = params.quality or self.quality
  local mod_options = params.mod_options or self.mod_options

  local sub_data_bid
  if params.owner_bid then
    sub_data_bid = params.sub_data_bid
  else
    sub_data_bid = self.sub_data_bid
  end

  local rotation = params.rotation or self.rotation

  return FixtureData(
    self.building_id,
    self.bid,
    self.uri,
    quality,
    owner_bid,
    origin,
    direction,
    masked,
    sub_data_bid,
    rotation,
    mod_options)
end

return LostEmsFixtureData
