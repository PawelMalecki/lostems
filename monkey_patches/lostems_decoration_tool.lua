-- local entity_forms_lib = require 'stonehearth.lib.entity_forms.entity_forms_lib'
-- local Point2 = _radiant.csg.Point2
local Point3 = _radiant.csg.Point3
-- local Region3 = _radiant.csg.Region3
local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- local RoofData = require 'stonehearth.lib.building.roof_data'
-- local RoomData = require 'stonehearth.lib.building.room_data'

local bindings = _radiant.client.get_binding_system()
local build_util = require 'stonehearth.lib.build_util'
local mutation_utils = require 'stonehearth.lib.building.mutation_utils'
local fixture_utils = require 'stonehearth.lib.building.fixture_utils'

local DecorationTool = radiant.mods.require('stonehearth.services.client.building.decoration_tool')
--local log = radiant.log.create_logger('decoration_tool')

local LostEmsDecorationTool = radiant.class()

function LostEmsDecorationTool:_populate_mod_options()
   self._mod_options = self._mod_options or {}
   if self._fixture_uri then
      local skin_info = radiant.entities.get_entity_data(self._fixture_uri, 'lostems:skin_info')
      if skin_info and skin_info.skins and (self._mod_options.skin_name == nil or skin_info.skins[self._mod_options.skin_name] == nil) then
         self._mod_options.skin_name = skin_info.default_skin or false
      end
   end
end

LostEmsDecorationTool._old_lostems__init = DecorationTool.__init
function LostEmsDecorationTool:__init(response, uri, quality, options)
   -- print('DecorationTool:__init(): ' .. uri)
   self:_populate_mod_options()
   -- print(self._mod_options)

   return self:_old_lostems__init(response, uri, quality)
end

function LostEmsDecorationTool:_commit()
   -- print('DecorationTool:_commit(): ' .. self._data:get_fixture_uri())
   -- print(self._data:get_mod_options())
   stonehearth.building:add_fixture(
      self._data:get_owner_bid(),
      self._data:get_fixture_uri(),
      self._data:get_quality(),
      self._data:get_origin(),
      self._data:get_direction(),
      self._data:get_owner_sub_bid(),
      self._data:get_rotation(),
      self._data:get_mod_options()
   )
end

function LostEmsDecorationTool:_set_fixture_data(owner, location_w, normal, rotation)
   -- print('DecorationTool:_set_fixture_data(): ' .. self._fixture_uri)

   if not location_w then
      return
   end

   local owner_bid = -1
   local owner_sub_bid = nil
   if radiant.entities.get_entity_data(owner, 'stonehearth:build2:widget') then
      local c = owner:get(radiant.entities.get_entity_data(owner, 'stonehearth:build2:widget').component)
      owner_bid = c:get_data():get_bid()

      if c:get_data().get_wall_id then
         owner_sub_bid = c:get_data():get_wall_id()
      end
   end

   local origin = Point3.zero
   if owner_bid ~= -1 then
      local owner_data = stonehearth.building:get_data(owner_bid)
      origin = owner_data:get_origin()
   end
   if normal.y == 0 then
      rotation = build_util.normal_to_rotation(normal)
   end

   if owner_bid > 0 and stonehearth.building:get_data(owner_bid):get_building_id() ~= stonehearth.building:get_current_building_id() then
      return
   end

   self:_populate_mod_options()
   -- print(self._mod_options)

   self._data = FixtureData.Make(
      stonehearth.building:get_current_building_id(),
      self._bid,
      self._fixture_uri,
      self._quality,
      owner_bid,
      location_w - origin,
      normal,
      owner_sub_bid,
      rotation,
      self._mod_options)

   if not self._blueprint then
      self._blueprint = radiant.entities.create_entity('stonehearth:build2:entities:fixture_blueprint', self._data)
      self._bp_c = self._blueprint:get('stonehearth:build2:blueprint')
      self._bp_c:init(self._data)
      stonehearth.building:add_blueprint(self._blueprint)

      self._widget = radiant.entities.create_entity(self._data:get_fixture_uri())
      self._widget:add_component('destination')
      self._widget:add_component('stonehearth:ui:widget')
      self._widget:add_component('stonehearth:build2:fixture_widget'):from_blueprint(self._bp_c)
   else
      self._bp_c:update_data(self._data)
   end

   stonehearth.building:begin_commit()
   mutation_utils.create(self._data)

   local modified, added, removed = stonehearth.building:get_all_mutation_data()

   stonehearth.building:end_commit()

   -- Take all the data, and commit it!
   for id, data in pairs(modified) do
      local widget = stonehearth.building:get_widget(id)
      -- It's actually possible for this code to race against widget creation,
      -- so even though the data is there, make sure we don't call preview on something
      -- that doesn't yet exist on the client in visual form!
      if widget then
         widget:preview_data(data)
      end
   end

   for id, data in pairs(added) do
      local widget = stonehearth.building:get_widget(id)
      if widget then
         widget:preview_data(data)
      end

      if id == self._data:get_bid() then
         self._data = data
      end
   end
end

-- handles keyboard events from the input service
-- Use comma and period to rotate the item
LostEmsDecorationTool._lostems_old_on_keyboard_event = DecorationTool._on_keyboard_event
function LostEmsDecorationTool:_on_keyboard_event(e)
   local event_consumed = false

   if bindings:is_action_active('build:change_skin') then
      self:_set_next_skin()
      self:_location_filter(self._brick, self._entity, self._normal, self._rotation)
      event_consumed = true
   end

   return event_consumed or self:_lostems_old_on_keyboard_event(e)
end

function LostEmsDecorationTool:_set_next_skin()
   -- print('LostEmsDecorationTool:_set_next_skin(): ' .. tostring(self._widget))
   if self._widget then
      local skin_changer = self._widget:get_component('lostems:skin_changer')
      if skin_changer then
         self._mod_options = self._mod_options or {}
         self._mod_options.skin_name = skin_changer:apply_next_skin(self._mod_options.skin_name)
      end
   end
   -- print(self._mod_options)
end

return LostEmsDecorationTool
