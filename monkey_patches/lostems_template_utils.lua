-- local Point2 = _radiant.csg.Point2
local Point3 = _radiant.csg.Point3
-- local Quaternion = _radiant.csg.Quaternion
-- local Region3 = _radiant.csg.Region3
-- local PolygonBuilder = _radiant.csg.PolygonBuilder

local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- local RoofData = require 'stonehearth.lib.building.roof_data'
-- local RoomData = require 'stonehearth.lib.building.room_data'
-- local StairsData = require 'stonehearth.lib.building.stairs_data'
-- local WallData = require 'stonehearth.lib.building.wall_data'
-- local BlocksData = require 'stonehearth.lib.building.blocks_data'
-- local PerimeterWallData = require 'stonehearth.lib.building.perimeter_wall_data'
-- local WallMap = require 'stonehearth.lib.building.wall_map'
-- local RoadData = require 'stonehearth.lib.building.road_data'

-- local mutation_utils = require 'stonehearth.lib.building.mutation_utils'
-- local entity_forms_lib = require 'stonehearth.lib.entity_forms.entity_forms_lib'
-- local build_util = require 'stonehearth.lib.build_util'
-- local fixture_utils = require 'stonehearth.lib.building.fixture_utils'
-- local region_utils = require 'stonehearth.lib.building.region_utils'
-- local log = radiant.log.create_logger('build.template_utils')

local template_utils = require 'stonehearth.lib.building.template_utils'
local lostems_template_utils = {}

local _compatiblity_json = radiant.resources.load_json('lostems:compatibility:building_templates')

--[[--
local function _clean_bid(map, bidmap)
   local new_map = {}

   for bid, v in pairs(map) do
      local n = tonumber(bid)
      local bid = bidmap[n]
      if bid then
         new_map[bid] = v
      end
   end
   return new_map
end
--]]--
--[[--
local function _tuple_to_point3(tuple)
   --strip first and last char
   tuple = string.sub(tuple, 2, tuple:len() - 1)
   --split by ','
   local pts = radiant.util.split_string(tuple, ',')
   pts[2] = string.sub(pts[2], 2)
   pts[3] = string.sub(pts[3], 2)

   return Point3(tonumber(pts[1]), tonumber(pts[2]), tonumber(pts[3]))
end
--]]--
local function _to_point3(json)
   return Point3(tonumber(json.x), tonumber(json.y), tonumber(json.z))
end

--[[--
local function _to_point2(json)
   return Point2(tonumber(json.x), tonumber(json.y))
end
--]]--
--[[--
local function _to_polygon(json)
   local p = PolygonBuilder()
   for _, pt in ipairs(json) do
      p:add_point(_to_point2(pt))
   end
   return p:build()
end
--]]--
--[[--
local function _to_region3(json)
   local r = Region3()
   r:load(json)
   return r
end
--]]--
--[[--
local function _json_to_data(json, fixtures, building_id, bidmap)
   local data
   if json.__classname == 'stonehearth:RoomData' then
      data = RoomData_FromJson(json, building_id, bidmap)
   elseif json.__classname == 'stonehearth:RoofData' then
      data = RoofData_FromJson(json, building_id, bidmap)
   elseif json.__classname == 'stonehearth:FixtureData' then
      data = FixtureData_FromJson(json, building_id, bidmap)
      fixtures[data:get_bid()] = data
   elseif json.__classname == 'stonehearth:WallData' then
      data = WallData_FromJson(json, building_id, bidmap)
   elseif json.__classname == 'stonehearth:StairsData' then
      data = StairsData_FromJson(json, building_id, bidmap)
   elseif json.__classname == 'stonehearth:BlocksData' then
      data = BlocksData_FromJson(json, building_id, bidmap)
   elseif json.__classname == 'stonehearth:RoadData' then
      data = RoadData_FromJson(json, building_id, bidmap)
   end

   return data
end
--]]--
--[[--
local function _json_to_blueprint(json, fixtures, building_id, bidmap, offset)
   local data = _json_to_data(json, fixtures, building_id, bidmap)

   if not data then
      return nil
   end

   if not data.get_owner_bid or data:get_owner_bid() == -1 then
      data.origin = data.origin + offset
   end

   if data:get_uri() ~= FixtureData.URI then
      local bp = radiant.entities.create_entity(data:get_uri())
      bp:get('stonehearth:build2:blueprint'):init(data)

      return bp
   end

   return nil
end
--]]--
--[[--
local function _collect_bids_from_data(json, bidmap)
   bidmap[tonumber(json.bid)] = 0
   if json.__classname == 'stonehearth:RoomData' or json.__classname == 'stonehearth:RoofData' then
      for pt, walls in pairs(json.wall_map.lookup) do
         if pt ~= 'size' then
            for _, wall in ipairs(walls) do
               bidmap[tonumber(wall.wall_id)] = 0
            end
         end
      end
   end
end
--]]--
--[[--
local function _bidmap_from_json(json_root, base_id)
   local bidmap = {}

   for _, json in pairs(json_root) do
      _collect_bids_from_data(json, bidmap)
   end

   local sorted_keys = radiant.keys(bidmap)
   table.sort(sorted_keys, function(a, b)
         return a < b
      end)

   for i, k in ipairs(sorted_keys) do
      bidmap[k] = i + base_id
   end

   return bidmap
end
--]]--

function FixtureData_FromJson(json, building_id, bidmap)
   local b = BuildingData_FromJson(json, bidmap)
   local uri = json.uri
   local owner_bid = tonumber(json.owner_bid)
   if owner_bid ~= -1 then
      owner_bid = bidmap[owner_bid]
   end
   local direction = _to_point3(json.direction)
   local sub_data_bid
   if json.sub_data_bid then
      sub_data_bid = bidmap[tonumber(json.sub_data_bid)]
   end
   local rotation = tonumber(json.rotation)
   local quality = json.quality and tonumber(json.quality) or -1
   local mod_options = json.mod_options or {}

   if _compatiblity_json.on_load[uri] and _compatiblity_json.on_load[uri].uri then
      mod_options.skin_name = _compatiblity_json.on_load[uri].skin_name or false
      uri = _compatiblity_json.on_load[uri].uri
   end

   return FixtureData(building_id, b.bid, uri, quality, owner_bid, b.origin, direction, b.masked, sub_data_bid, rotation, mod_options)
end

lostems_template_utils._old_lostems_get_template_save_data = template_utils.get_template_save_data
function lostems_template_utils.get_template_save_data(building, template_id)
   local template, template_id = lostems_template_utils._old_lostems_get_template_save_data(building, template_id)

   for bid, data in pairs(template.data) do
      if data.mod_options and data.mod_options.skin_name and _compatiblity_json.on_save[data.uri] and _compatiblity_json.on_save[data.uri][data.mod_options.skin_name] then
         template.data[bid].uri = _compatiblity_json.on_save[data.uri][data.mod_options.skin_name]
         template.data[bid].mod_options.skin_name = nil
      end
   end

   return template, template_id
end

--[[--
lostems_template_utils._old_lostems_load_template_from_data = template_utils.load_template_from_data
function lostems_template_utils.load_template_from_data(building, template_id, base_id, offset, rot_point, rotation, template)
   print('lostems_template_utils.load_template_from_data() called.')
   local fixtures = {}

   local bc = building:get('stonehearth:build2:building')

   -- Generate a new template id, so that (once placed), changing this building doesn't
   -- modify the original template.
   bc:set_template_id(_radiant.sim.generate_uuid())
   bc:set_revision(1)
   bc:set_template_icon(template.header.preview_image)

   if template.header.custom_name then
      radiant.entities.set_custom_name(building, template.header.custom_name)
   end

   template.header.sunk = template.header.sunk ~= nil and template.header.sunk or false

   if not template.data then
      return building:get_id(), false
   end

   local bidmap = _bidmap_from_json(template.data, base_id)

   stonehearth.building:set_next_bid(radiant.size(bidmap) + base_id + 1)

   local building_id = building:get_id()
   for _, json in pairs(template.data) do
      local bp = _json_to_blueprint(json, fixtures, building_id, bidmap, offset)

      if bp then
         bc:add_blueprint(bp)
         stonehearth.building:add_blueprint(bp)
      end
   end

   -- Fixtures must come after host structures.
   for _, data in pairs(fixtures) do
      local bp = radiant.entities.create_entity(data:get_uri())
      bp:get('stonehearth:build2:blueprint'):init(data)
      bc:add_blueprint(bp)
      stonehearth.building:add_blueprint(bp)
   end

   print('1st loop. fixtures:')
   for _, bp in bc:get_blueprints():each() do
      local data = bp:get('stonehearth:build2:blueprint'):get_data()
      if data:get_uri() == FixtureData.URI then
         print(data)
      end
   end

   -- Finally, do the rotation fixup (now that we can do our data lookups)

   if rotation and rotation ~= 0 then
      for _, bp in bc:get_blueprints():each() do
         local data = bp:get('stonehearth:build2:blueprint'):get_data()
         data:dangerous_rotate(rot_point, rotation)
      end
   end

   print('2nd loop. fixtures:')
   for _, bp in bc:get_blueprints():each() do
      local data = bp:get('stonehearth:build2:blueprint'):get_data()
      if data:get_uri() == FixtureData.URI then
         print(data)
      end
   end

   -- For giggles, lets do a transitivity inspection here.
   for _, bp in bc:get_blueprints():each() do
      local data = bp:get('stonehearth:build2:blueprint'):get_data()
      local bid = data:get_bid()

      for other_bid, _ in pairs(data:get_supports()) do
         if not stonehearth.building:has_data(other_bid) then
            data.supports[other_bid] = nil
         else
            local other = stonehearth.building:get_data(other_bid)
            if not (other:get_supported()[bid]) then
               log:error('supports transitivity error: %s -> %s', bid, other_bid)
               data.supports[other_bid] = nil
            end
         end
      end

      for other_bid, _ in pairs(data:get_supported()) do
         if not stonehearth.building:has_data(other_bid) then
            data.supported[other_bid] = nil
         else
            local other = stonehearth.building:get_data(other_bid)
            if not (other:get_supports()[bid]) then
               log:error('supported transitivity error: %s -> %s', bid, other_bid)
               data.supported[other_bid] = nil
            end
         end
      end

      for other_bid, _ in pairs(data:get_masks()) do
         if not stonehearth.building:has_data(other_bid) then
            data.masks[other_bid] = nil
         else
            local other = stonehearth.building:get_data(other_bid)
            if not (other:get_masked()[bid]) then
               log:error('masks transitivity error: %s -> %s', bid, other_bid)
               data.masks[other_bid] = nil
            end
         end
      end

      for other_bid, _ in pairs(data:get_masked()) do
         if not stonehearth.building:has_data(other_bid) then
            data.masked[other_bid] = nil
         else
            local other = stonehearth.building:get_data(other_bid)
            if not (other:get_masks()[bid]) then
               log:error('masked transitivity error: %s -> %s', bid, other_bid)
               data.masked[other_bid] = nil
            end
         end
      end

      for other_bid, _ in pairs(data:get_adjacent()) do
         if not stonehearth.building:has_data(other_bid) then
            data.adjacent[other_bid] = nil
         else
            local other = stonehearth.building:get_data(other_bid)
            if not (other:get_adjacent()[bid]) then
               log:error('adjacent transitivity error: %s -> %s', bid, other_bid)
               data.adjacent[other_bid] = nil
            end
         end
      end
   end

   -- Finally, do the deferred building.
   for _, bp in bc:get_blueprints():each() do
      local data = bp:get('stonehearth:build2:blueprint'):get_data()
      bp:get('stonehearth:build2:blueprint'):update_data(data:deferred_build())
   end

   building:get('stonehearth:build2:building'):reset_costs()

   print('3rd loop. fixtures:')
   for _, bp in bc:get_blueprints():each() do
      local data = bp:get('stonehearth:build2:blueprint'):get_data()
      if data:get_uri() == FixtureData.URI then
         print(data)
      end
   end

   return building:get_id(), template.header.sunk
end
--]]--

--ALL GODS THIS MONSTER SHOULD BE SPLIT INTO PARTS SO PEOPLE CAN ACTUALLY PATCH IT!
--compare with TempBuilding:finish()
function lostems_template_utils.save_building_template_screenshot(building, template_name, done_cb)

   local function create_offscreen_building(building)
      local function create_structure(d, owner)
         local structure = radiant.entities.create_entity('stonehearth:build2:entities:temp_structure', { owner = owner })
         local wr = d:get_world_real_region()
         radiant.terrain.place_entity_at_exact_location(structure, Point3(0, 0, 0), {root_entity = owner})
         structure:get('destination'):get_region():modify(function(cursor)
               cursor:copy_region(wr)
            end)
         -- TODO: ugh, fix this when we get proper ownership.
         if d.get_wall_map then
            for _, walls in d:get_wall_map():each() do
               for _, wall in pairs(walls) do
                  create_structure(wall, owner)
               end
            end
         end
      end

      --LostEms: wrapping it into a function as well because why not?
      local function create_fixture(d, owner)
         local f = radiant.entities.create_entity(d:get_fixture_uri(), {
            owner = owner })
         radiant.terrain.place_entity_at_exact_location(f, d:get_world_origin(), {
               root_entity = owner,
               force_iconic = false,
            })
         radiant.entities.turn_to(f, d:get_rotation())
         local mod_options = (d.get_mod_options ~= nil and d:get_mod_options() or d.mod_options)
         if mod_options and mod_options.skin_name then
            local skin_changer = f:get_component('lostems:skin_changer')
            if skin_changer then skin_changer:apply_skin(mod_options.skin_name) end
         end
      end

      local offscreen_building = radiant.entities.create_entity('stonehearth:build2:entities:temp_building')

      for _, bp in building:get('stonehearth:build2:building'):get_blueprints():each() do
         local d = bp:get('stonehearth:build2:blueprint'):get_data()
         if d:get_uri() == FixtureData.URI then
            create_fixture(d, offscreen_building)
         else
            create_structure(d, offscreen_building)
         end
      end

      return offscreen_building
   end

   local function position_camera(camera, camera_direction, bounds)
      local center = (bounds.min + bounds.max):scaled(0.5)
      local girth = (bounds.max - bounds.min):length()

      local camera_distance = girth * 1.6
      local camera_offset = camera_direction:scaled(camera_distance)
      local position = center + camera_offset

      camera:set_is_orthographic(false)
      camera:set_position(position)
      camera:look_at(center)
      camera:set_fov(35)
   end


   local function get_camera_and_light(cam_pos, world_bounds)
      -- Woo, awful code!  The idea behind this: we don't know what the 'front' of the building
      -- is, so we leave it up to the user, but we constrain the kind of picture we take.  So,
      -- figure out what quadrant around the building the are in (front/back/left/right, or the diagonals.)

      -- These are 'hand-crafted' vectors of camera/light directions for each face of the building.
      local props = {
         {
            camera_direction = Point3(3, 2, -4),
            light_direction = Point3(-50, 140, 0)
         },
         {
            camera_direction = Point3(-3, 2, 4),
            light_direction = Point3(220, 180, 0)
         },
         {
            camera_direction = Point3(-4, 2, -3),
            light_direction = Point3(210, 90, 0)
         },
         {
            camera_direction = Point3(4, 2, 3),
            light_direction = Point3(-50, 90, 0)
         }
      }

      local p = props[1]

      local x_quad = 0
      if cam_pos.x > world_bounds.min.x then
         if cam_pos.x <= world_bounds.max.x then
            x_quad = 0
         else
            x_quad = 1
         end
      else
         x_quad = -1
      end
      local z_quad = 0
      if cam_pos.z > world_bounds.min.z then
         if cam_pos.z <= world_bounds.max.z then
            z_quad = 0
         else
            z_quad = 1
         end
      else
         z_quad = -1
      end

      -- These four cases are the simple ones, where if you're clearly at one side of the building
      -- you just take the picture from that side.
      if x_quad == 0 then
         if z_quad == -1 then
            p = props[1]
         elseif z_quad == 1 then
            p = props[2]
         end
      elseif z_quad == 0 then
         if x_quad == -1 then
            p = props[3]
         elseif x_quad == 1 then
            p = props[4]
         end
      else
         -- These are the awkward cases, where you're at one of the diagonal quadrants.  Figure out
         -- what edge of the quadrant you're closest to, in order to select the appropriate face
         -- of the building from which to take a picture.
         if z_quad == 1 then
            if x_quad == 1 then
               if cam_pos.x - world_bounds.max.x > cam_pos.z - world_bounds.max.z then
                  p = props[4]
               else
                  p = props[2]
               end
            else
               if world_bounds.min.x - cam_pos.x > cam_pos.z - world_bounds.max.z then
                  p = props[3]
               else
                  p = props[2]
               end
            end
         else
            if x_quad == 1 then
               if cam_pos.x - world_bounds.max.x > world_bounds.min.z - cam_pos.z then
                  p = props[4]
               else
                  p = props[1]
               end
            else
               if world_bounds.min.x - cam_pos.x > world_bounds.min.z - cam_pos.z then
                  p = props[3]
               else
                  p = props[1]
               end
            end
         end
      end

      p.camera_direction:normalize()
      return p.camera_direction, p.light_direction
   end

   local bounds_w = building:get('stonehearth:build2:building'):get_blueprint_bounds()

   if bounds_w:get_area() == 0 then
      done_cb()
      return
   end

   local camera_direction, light_direction = get_camera_and_light(
      stonehearth.camera:get_position(),
      bounds_w)

   local offscreen_building = create_offscreen_building(building)
   local building_render_entity, light

   _radiant.client.render_staged_scene(800, function(scene_root, camera)
         building_render_entity = _radiant.client.create_render_entity(scene_root, offscreen_building)

         position_camera(camera, camera_direction, bounds_w)

         light = scene_root:add_directional_light('fake sun')
         light:set_radius_2(10000)
         light:set_fov(360)
         light:set_shadow_map_count(4)
         light:set_shadow_split_lambda(0.95)
         light:set_shadow_map_bias(0.001)
         light:set_color(Point3(0.75, 0.66, 0.75))
         light:set_ambient_color(Point3(0.35,  0.35, 0.35))
         light:set_transform(0, 0, 0, light_direction.x, light_direction.y, light_direction.z, 1, 1, 1)
      end, function()
         if building_render_entity then
            building_render_entity:destroy()
            building_render_entity = nil
         end
         if light then
            light:destroy()
            light = nil
         end
         if offscreen_building then
            radiant.entities.destroy_entity(offscreen_building)
            offscreen_building = nil
         end
      end, function(bytes)
         local template_path =  'building_templates/'.. template_name
         local image_name = 'stonehearth/' .. template_path
         _radiant.client.save_offscreen_image(image_name, bytes)

         local template = _radiant.res.get_custom_building_template(template_name)
         template.header.preview_image = '/r/saved_objects/stonehearth/building_templates/' .. template_name .. '.png'
         _radiant.res.write_custom_building_template(template_name, template)

         if done_cb then
            done_cb()
         end
      end)
end

return lostems_template_utils
