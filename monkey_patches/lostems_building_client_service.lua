-- local TraceCategories = _radiant.dm.TraceCategories
-- local Point3 = _radiant.csg.Point3
-- local Color4 = _radiant.csg.Color4
-- local Region3 = _radiant.csg.Region3

-- local SelectionSet = require 'stonehearth.services.client.widget.selection_set'

-- local EraseTool = require 'stonehearth.services.client.building.erase_tool'
-- local BlocksTool = require 'stonehearth.services.client.building.blocks_tool'
-- local RoomTool = require 'stonehearth.services.client.building.room_tool'
-- local WallTool = require 'stonehearth.services.client.building.wall_tool'
-- local SplitWallTool = require 'stonehearth.services.client.building.split_wall_tool'
-- local DecorationTool = require 'stonehearth.services.client.building.decoration_tool'
-- local RoofTool = require 'stonehearth.services.client.building.roof_tool'
-- local PaintTool = require 'stonehearth.services.client.building.paint_tool'
-- local HoleTool = require 'stonehearth.services.client.building.hole_tool'
-- local StairsTool = require 'stonehearth.services.client.building.stairs_tool'
-- local TemplatePlacementTool = require 'stonehearth.services.client.building.template_placement_tool'
-- local PointerTool = require 'stonehearth.services.client.building.pointer_tool'
-- local RoadTool = require 'stonehearth.services.client.building.road_tool'
-- local ColorPickerTool = require 'stonehearth.services.client.building.colorpicker_tool'

-- local bindings = _radiant.client.get_binding_system()
-- local log = radiant.log.create_logger('building_service')
-- local WallData = require 'stonehearth.lib.building.wall_data'
-- local BlocksData = require 'stonehearth.lib.building.blocks_data'
-- local RoomData = require 'stonehearth.lib.building.room_data'
-- local RoofData = require 'stonehearth.lib.building.roof_data'
-- local StairsData = require 'stonehearth.lib.building.stairs_data'
-- local RoadData = require 'stonehearth.lib.building.road_data'
-- local PerimeterWallData = require 'stonehearth.lib.building.perimeter_wall_data'

-- local template_utils = require 'stonehearth.lib.building.template_utils'
local build_util = require 'stonehearth.lib.build_util'

-- local BlockKinds = stonehearth.constants.building.block_kinds

-- local Building = 'stonehearth:build2:building'

--[[
local function _entities_to_bids(entities)
   local bids = {}

   for _, w in pairs(entities) do
      local d
      if w:get('stonehearth:build2:fixture_widget') then
         d = w:get('stonehearth:build2:fixture_widget'):get_data()
      else
         local c = radiant.entities.get_entity_data(w, 'stonehearth:build2:widget').component
         d = w:get(c):get_data()
      end
      bids[d:get_bid()] = true
   end
   return bids
end
]]--

local BuildingService = require 'stonehearth.services.client.building.building_client_service'
local LostEmsBuildingService = class()

--LostEmsBuildingService._old_lostems_add_fixture = BuildingService.add_fixture
function LostEmsBuildingService:add_fixture(owner_bid, fixture_uri, quality, offset, direction, owner_sub_bid, rotation, mod_options)
   build_util.play_fixture_build_sound(fixture_uri, rotation)
   -- print('BuildingService:add_fixture(): ' .. fixture_uri)
   -- print(mod_options)
   self:_issue_command('add_fixture_command', nil, owner_bid, fixture_uri, quality, offset, direction, owner_sub_bid, rotation, mod_options)
end

return LostEmsBuildingService
