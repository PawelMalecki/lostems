-- local Region3 = _radiant.csg.Region3
-- local Cube3 = _radiant.csg.Cube3
-- local Point3 = _radiant.csg.Point3
-- local Color4 = _radiant.csg.Color4
-- local TraceCategories = _radiant.dm.TraceCategories
-- local bindings = _radiant.client.get_binding_system()
-- local fixture_utils = require 'stonehearth.lib.building.fixture_utils'
-- local FixtureData = require 'stonehearth.lib.building.fixture_data'
-- local log = radiant.log.create_logger('build.fixture_widget')
-- local Widget = 'stonehearth:ui:widget'
-- local FootprintWidget = require 'stonehearth.services.client.selection.footprint_widget'

local FixtureWidget = require 'stonehearth.services.client.widget.fixture_widget'
local LostEmsFixtureWidget = class()

LostEmsFixtureWidget._old_lostems_preview_data = FixtureWidget.preview_data
function LostEmsFixtureWidget:preview_data(data)
   -- print('FixtureWidget:preview_data(): ' .. tostring(self._entity))
   local result = self:_old_lostems_preview_data(data)
   local mod_options = (data.get_mod_options ~= nil) and data:get_mod_options() or data.mod_options
   if mod_options and mod_options.skin_name then
      local skin_changer = self._entity:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(mod_options.skin_name) end
   end
   return result
end

LostEmsFixtureWidget._old_lostems_update_data = FixtureWidget.update_data
function LostEmsFixtureWidget:update_data(data)
   -- print('FixtureWidget:update_data(): ' .. tostring(self._entity))
   local result = self:_old_lostems_update_data(data)
   local mod_options = (data.get_mod_options ~= nil) and data:get_mod_options() or data.mod_options
   if mod_options and mod_options.skin_name then
      local skin_changer = self._entity:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(mod_options.skin_name) end
   end
   return result
end

return LostEmsFixtureWidget
