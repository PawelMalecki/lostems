-- local build_util = require 'stonehearth.lib.build_util'
local entity_forms = require 'stonehearth.lib.entity_forms.entity_forms_lib'

-- local Point3 = _radiant.csg.Point3
local Entity = _radiant.om.Entity
-- local TraceCategories = _radiant.dm.TraceCategories
-- local log = radiant.log.create_logger('entity_forms')

-- local MOVE_ITEM_COMMAND = 'stonehearth:commands:move_item'
-- local UNDEPLOY_COMMAND = 'stonehearth:commands:undeploy_item'

local EntityFormsComponent = require 'stonehearth.components.entity_forms.entity_forms_component'

local LostEmsEntityFormsComponent = class()

function LostEmsEntityFormsComponent:place_item_on_structure(location, structure_entity, rotation, normal, mod_options)
   assert(structure_entity, 'no structure in :place_item_on_structure()')
   assert(normal, 'no normal in :place_item_on_structure()')
   assert(radiant.util.is_a(structure_entity, Entity), 'structure is not an entity in place_item_on_structure')
   assert(self:is_placeable(), 'cannot place non-placeable item')

   -- cancel whatever pending tasks we had earliers.
   self:_cleanup_item_placement()

   -- rememeber what we're doing now so we can early-exit later if necessary.
   self._sv.placement_info = {
      is_entity_placement = true,
      structure = structure_entity,
      normal = normal,
      location = location,
      rotation = rotation,
      mod_options = mod_options
   }
   self.__saved_variables:mark_changed()

   -- make sure the structure is finished, first.  the fixture fabricator should have
   -- managed this dependency for us!
   local structure_cp = structure_entity:get_component('stonehearth:construction_progress')
   radiant.assert(not structure_cp or structure_cp:get_finished(), "Attempting to place %s on a non-finished building %s!", self._entity, structure_entity)

   self:_create_pick_up_ladder_task()
   self:_create_put_down_ladder_task(location, normal)

   -- finally create a placement task to move the item from one spot to another
   self:_place_item()
end

function LostEmsEntityFormsComponent:place_item_on_ground(location, rotation, normal, mod_options)
   assert(self._sv.iconic_entity, 'cannot place items with no iconic entity. Entity: ' .. tostring(self._entity))

   self:_cleanup_item_placement()

   self._sv.placement_info = {
      is_entity_placement = true,
      structure = radiant._root_entity,
      normal = normal,
      location = location,
      rotation = rotation,
      mod_options = mod_options
   }
   self.__saved_variables:mark_changed()
   self:_create_pick_up_ladder_task()
   self:_create_put_down_ladder_task(location, normal)

   self:_place_item()
end

function LostEmsEntityFormsComponent:_place_item()
   self:set_should_restock(false) -- if we're gonna place this item, don't restock it. That would be rude

   local town = stonehearth.town:get_town(self._entity)

   assert(town)
   assert(self._sv.placement_info)
   assert(not self._sv.placement_ghost_entity)

   --ask the town to cancel any  other existing tasks on the root and the iconic
   town:remove_town_tasks_on_item(self._entity)
   town:remove_town_tasks_on_item(self:get_iconic_entity())

   local uri = self._entity:get_uri()
   local player_id = radiant.entities.get_player_id(self._entity)
   local ghost, err = entity_forms.place_ghost_entity(uri, radiant.entities.get_item_quality(self._entity), player_id, self._sv.placement_info)

   assert(ghost, err)
   if self._sv.placement_info and self._sv.placement_info.mod_options and self._sv.placement_info.mod_options.skin_name then
      local skin_changer = ghost:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(self._sv.placement_info.mod_options.skin_name) end
   end

   -- if we hide the undeploy ui, then we have to prevent cancelling the move as that hacks an undeploy
   if self._sv.hide_undeploy_ui then
      ghost:get_component('stonehearth:commands'):remove_command('stonehearth:commands:destroy_item')
   end
   self._sv.placement_ghost_entity = ghost
   radiant.events.trigger_async(self, 'stonehearth:entity:ghost_placed')

   local ghost_component = ghost:get_component('stonehearth:ghost_form')
   -- if root form item exists in world, then the item is placed and we are trying to move it
   local moving_placed_item = radiant.entities.exists_in_world(self._entity)
   ghost_component:request_place_item(moving_placed_item)

   local inventory = stonehearth.inventory:get_inventory(self._entity)
   if inventory then
      inventory:reevaluate_tracker_item(self._entity)
      inventory:reevaluate_tracker_item(self._sv.iconic_entity)
   end

   self:_start_placement_task()
end

return LostEmsEntityFormsComponent
