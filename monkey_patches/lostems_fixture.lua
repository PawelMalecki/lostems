local log = radiant.log.create_logger('build.fixture')

local Fixture = require 'stonehearth.components.building2.fixture'
local LostEmsFixture = class()

LostEmsFixture._old_lostems_set_data = Fixture.set_data
function LostEmsFixture:set_data(building, bid, uri, quality, placement_info, collision_type)
   -- print('Fixture:set_data(): ' .. tostring(self._entity))
   -- print(placement_info)
   local result = self:_old_lostems_set_data(building, bid, uri, quality, placement_info, collision_type)

   if placement_info.mod_options and placement_info.mod_options.skin_name then
      local skin_changer = self._entity:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(placement_info.mod_options.skin_name) end
   end

   return result
end

LostEmsFixture._old_lostems_set_entity_placed = Fixture.set_entity_placed
function LostEmsFixture:set_entity_placed(entity)
   -- print('Fixture:set_entity_placed('.. tostring(entity)..'): placement_info =')
   -- print(self._sv._placement_info)
   if self._sv._placement_info.mod_options and self._sv._placement_info.mod_options.skin_name then
      local skin_changer = entity:get_component('lostems:skin_changer')
      if skin_changer then skin_changer:apply_skin(self._sv._placement_info.mod_options.skin_name) end
   end

   return self:_old_lostems_set_entity_placed(entity)
end

return LostEmsFixture
