local ItemPlacer = require 'stonehearth.services.client.build_editor.item_placer'
--local build_util = require 'stonehearth.lib.build_util'
--local entity_forms_lib = require 'stonehearth.lib.entity_forms.entity_forms_lib'
local LostEmsItemPlacer = class()

--LostEmsItemPlacer._old_lostems_done_fn = ItemPlacer._done_fn
function LostEmsItemPlacer:_done_fn(selector, location, rotation)
   -- print('ItemPlacer:_done_fn(): ' .. tostring(self.ghost_entity))
   -- print(self.location_selector)
   local mod_options = (selector._get_mod_options ~= nil and selector:_get_mod_options() or selector._mod_options) or {}
   -- print(mod_options)

   local deferred
   if self.placement_structure and self.placement_structure:get_id() ~= radiant._root_entity_id then
      if self.specific_item_to_place then
         deferred = _radiant.call('stonehearth:place_item_on_structure',
            self.specific_item_to_place, location, rotation, self.placement_structure,
            self.placement_structure_normal, self.transactional, mod_options)
      elseif self.options and self.options.add_craft_order then
         deferred = _radiant.call('stonehearth_ace:craft_and_place_item_type_in_world',
            self.item_uri_to_place, location, rotation, self.placement_structure_normal, self.placement_structure, mod_options)
      else
         deferred = _radiant.call('stonehearth:place_item_type_on_structure',
            self.item_uri_to_place, self.quality, location, rotation, self.placement_structure,
            self.placement_structure_normal, self.transactional, mod_options)
      end
   else
      if self.specific_item_to_place then
         deferred = _radiant.call('stonehearth:place_item_in_world',
            self.specific_item_to_place, location, rotation, self.placement_structure_normal, mod_options)
      elseif self.options and self.options.add_craft_order then
         deferred = _radiant.call('stonehearth_ace:craft_and_place_item_type_in_world',
            self.item_uri_to_place, location, rotation, self.placement_structure_normal, nil, mod_options)
      else
         deferred = _radiant.call('stonehearth:place_item_type_in_world',
            self.item_uri_to_place, self.quality, location, rotation, self.placement_structure_normal, mod_options)
      end
   end
   deferred
      :done(function (result)
            self.response:resolve(result)
         end)
      :fail(function(result)
            self.response:reject(result)
         end)
      :always(function ()
            radiant.entities.destroy_entity(self.ghost_entity)
            selector:destroy()
         end)
end

return LostEmsItemPlacer