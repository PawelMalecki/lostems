-- local Point3 = _radiant.csg.Point3
-- local build_util = require 'stonehearth.lib.build_util'
-- local entity_forms_lib = require 'stonehearth.lib.entity_forms.entity_forms_lib'
local GhostFormComponent = require 'stonehearth.components.ghost_form.ghost_form_component'

local LostEmsGhostFormComponent = class()

--Ghost form component is often added programmatically and does not exist while SkinChanger post_activate() is called
--Because of that we need to call the necessary function on SkinChanger component directly from ghost form component's post_activate()
LostEmsGhostFormComponent._old_lostems_post_activate = GhostFormComponent.post_activate
function LostEmsGhostFormComponent:post_activate()
  local result = self:_old_lostems_post_activate()
  if radiant.is_server then
    local skin_changer = self._entity:get_component('lostems:skin_changer')
    if skin_changer then skin_changer:_on_ghost_form_post_activated() end
  end
  return result
end

LostEmsGhostFormComponent._old_lostems_set_placement_info = GhostFormComponent.set_placement_info
function LostEmsGhostFormComponent:set_placement_info(placement_info)
  if self._sv.placement_info and self._sv.placement_info.options and self._sv.placement_info.options.skin_name then
    local skin_changer = self._entity:get_component('lostems:skin_changer')
    if skin_changer then
      skin_changer:apply_skin(self._sv.placement_info.options.skin_name)
    end
  end
  return self:_old_lostems_set_placement_info(placement_info)
end

return LostEmsGhostFormComponent